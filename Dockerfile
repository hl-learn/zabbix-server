ARG VERSION=18.04
FROM ubuntu:$VERSION
COPY . /tmp/
RUN apt-get update && apt-get install -y --no-install-recommends \
wget \
&& wget -P /tmp/ http://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+bionic_all.deb \
&& dpkg -i /tmp/zabbix-release_5.0-1+bionic_all.deb \
&& apt-get update \
&& apt-get install -y --no-install-recommends \
zabbix-server-pgsql \
&& mv /tmp/zabbix_server.conf /etc/zabbix/zabbix_server.conf \
&& mkdir /var/run/zabbix && chown zabbix:zabbix /var/run/zabbix \
&& apt-get remove -y wget
EXPOSE 10051
CMD ["/usr/sbin/zabbix_server", "--foreground", "-c", "/etc/zabbix/zabbix_server.conf"]
